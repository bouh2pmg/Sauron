FROM python:3
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN apt update && apt install -y postgresql-client

COPY run.sh run.sh
CMD ["./run.sh"]