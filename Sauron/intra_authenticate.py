import requests
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist

from Sauron.models import APIToken
from Sauron.settings import INTRA_AUTOLOGIN, SCRAPER_API_URL


def office_connect(username, password):
    payload = {'id': username, 'password': password}
    res = requests.post("https://console.bocal.org/auth/login", data=payload)
    return res.status_code == 200


def get_group(username):
    for group in Group.objects.all():
        res = requests.get("https://intra.epitech.eu/%s/group/%s/member?format=json" % (INTRA_AUTOLOGIN, group.name))
        if res.status_code / 100 == 2:
            for user in res.json():
                if user['login'] == username:
                    return group
    return None


def get_token(username, password):
    res = requests.post(SCRAPER_API_URL.format("/get_token"), data={'username': username, 'password': password})
    if res.status_code == 200:
        return res.json()['token']
    return None


def set_token(user, api_token):
    try:
        token = APIToken.objects.get(user=user)
    except:
        token = APIToken.objects.create(user=user)
    token.token = api_token
    token.save()


def authenticate(username=None, password=None):
    username = username.lower()
    token = get_token(username, password)
    if not username or not password or not token:
        return None
    if office_connect(username, password):
        group = get_group(username)
        if not group:
            return None
        try:
            user = User.objects.get(email=username)
            user.groups.add(group)
        except ObjectDoesNotExist:
            user = User.objects.create_user(username, username, password)
            user.first_name = username.split('.')[0]
            user.last_name = username.split('.')[1].split('@')[0]
            user.is_staff = True
            user.groups.add(group)
        user.save()
        set_token(user, token)
        return user
    for user in User.objects.filter(is_superuser=True):
        if user.username == username and check_password(password, user.password):
            set_token(user, token)
            return user
    return None


class ModelBackend:
    def authenticate(self, request, username=None, password=None):
        username = username.lower()
        token = get_token(username, password)
        if not request or not username or not password or not token:
            return None
        if office_connect(username, password):
            group = get_group(username)
            if not group:
                return None
            try:
                user = User.objects.get(email=username)
                user.groups.add(group)
            except ObjectDoesNotExist:
                user = User.objects.create_user(username, username, password)
                user.first_name = username.split('.')[0]
                user.last_name = username.split('.')[1].split('@')[0]
                user.is_staff = True
                user.groups.add(group)
            user.save()
            set_token(user, token)
            return user
        for user in User.objects.filter(is_superuser=True):
            if user.username == username and check_password(password, user.password):
                set_token(user, token)
                return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
