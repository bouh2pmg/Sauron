from django.contrib.auth.models import User
from django.db import models


class APIToken(models.Model):
    user = models.OneToOneField(User, unique=True, primary_key=True, on_delete=models.CASCADE)
    token = models.CharField(max_length=42)

    def __str__(self):
        return self.user.username + " : " + self.token
