from django.template.defaulttags import register


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter(name='split')
def split(value, arg):
    return value.split(arg)
