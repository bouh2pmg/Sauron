import requests
from django import template

from Sauron.settings import SCRAPER_API_URL

register = template.Library()


def _get_cursus_name(promo_cursus):
    cursus_list = {
        'PGE': ('bachelor', 'master'),
        'PGT': ('PGT',)
    }
    cursus = ""
    for c in cursus_list:
        for type in cursus_list[c]:
            if promo_cursus.startswith(type):
                cursus = c
                break
        if cursus != "":
            break
    return cursus


@register.simple_tag()
def get_promotions():
    res = requests.get(SCRAPER_API_URL.format("/promos"))
    print(res)
    return None
#    cursus = {}
#    for promo in Promo.objects.all().values('cursus', 'promotion_year').order_by('-promotion_year').distinct():
#        c = _get_cursus_name(promo['cursus'])
#        if c not in cursus:
#            cursus[c] = []
#        if promo['promotion_year'] not in cursus[c]:
#            cursus[c].append(promo['promotion_year'])
#    return cursus
