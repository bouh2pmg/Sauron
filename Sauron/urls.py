from django.conf.urls import url
from django.contrib import admin

from Sauron.settings import MAINTENANCE
from .views import *

urlpatterns = []

if MAINTENANCE:
    urlpatterns += [
        url(r'^maintenance$', maintenance_view),
        url(r'', maintenance_redirect)
    ]

urlpatterns += (
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    url(r'^login$', login_view),
    url(r'^logout$', logout_view),
    url(r'^api/(?P<route>[\d\w/\-]+)$', get_api_view),

    url(r'^roadblock$', roadblock_view),
    url(r'^module_validations$', module_validations_view),
    url(r'^module_alerts$', module_alerts_view),
    url(r'^tepitech_alerts$', tepitech_alerts_view),
)