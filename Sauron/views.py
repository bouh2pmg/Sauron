import requests
from django.contrib.auth import login, logout
from Sauron.intra_authenticate import authenticate
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils import timezone

from Sauron.models import APIToken
from Sauron.settings import SCRAPER_API_URL
from Sauron.roadblocks import roadblocks


def maintenance_redirect(req):
    return redirect('/maintenance')


def maintenance_view(req):
    return render(req, 'maintenance.html', locals())


def login_view(req):
    if req.user.is_authenticated:
        return redirect('/')
    if req.method == "POST" and 'username' in req.POST and 'password' in req.POST:
        user = authenticate(username=req.POST['username'], password=req.POST['password'])
        if user:
            login(req, user)
            return redirect('/')
        error = "Please enter the correct username and password"
    return render(req, 'login.html', locals())


@login_required(login_url="/login")
def logout_view(req):
    logout(req)
    return redirect('/')


@login_required(login_url="/login")
def index(req):
    return redirect('/roadblock')


def get_cities(token, year=True):
    params = {'format': 'json'}
    if year:
        params['promotion_year'] = timezone.now().year + (3 if timezone.now().month in range(8, 13) else 2)
    cities = []
    for cursus in ['bachelor/classic', 'bachelor/tek1ed', 'bachelor/tek2ed', 'bachelor/tek3s', 'master/classic',
                   'master/tek3si']:
        params['cursus'] = cursus
        try:
            res = requests.get(SCRAPER_API_URL.format("/promos"), headers={'Authorization': 'Token ' + token.token}, params=params, timeout=10)
            for promo in res.json():
                if promo['city'] not in cities:
                    cities.append(promo['city'])
        except:
            pass
    return sorted(cities, key=lambda k: k['name'])


@login_required(login_url="/login")
def roadblock_view(req):
    token = APIToken.objects.get(user=req.user)
    cities = get_cities(token)
    max_roadblock = max(len(roadblocks['innovation']), len(roadblocks['expertise']), len(roadblocks['softskills']))
    roadblocks_list = []
    for idx in range(max_roadblock):
        line = {}
        for roadblock in roadblocks:
            try:
                line[roadblock] = roadblocks[roadblock][idx]
            except:
                pass
        roadblocks_list.append(line)
    return render(req, 'tek3_roadblock.html', locals())


def get_api(req, route):
    token = APIToken.objects.get(user=req.user)
    res = requests.get(SCRAPER_API_URL.format(route), headers={'Authorization': 'Token ' + token.token}, params=dict(req.GET))
    if res.status_code == 200:
        return res.json()
    return []


@login_required(login_url='/login')
def get_api_view(req, route):
    return JsonResponse(get_api(req, '/' + route), safe=False)


@login_required(login_url="/login")
def module_validations_view(req):
    modules = get_api(req, '/modules')
    return render(req, 'module_validations.html', locals())


@login_required(login_url='/login')
def module_alerts_view(req):
    token = APIToken.objects.get(user=req.user)
    cities = get_cities(token, False)
    modules = get_api(req, '/modules')
    return render(req, 'module_alerts.html', locals())


@login_required(login_url='/login')
def tepitech_alerts_view(req):
    token = APIToken.objects.get(user=req.user)
    cities = get_cities(token, False)
    return render(req, 'tepitech_alerts.html', locals())
