#!/bin/bash

pg_isready -h db
while [ $? -ne 0 ]
do
    sleep 2
    pg_isready -h db
done

python3 manage.py migrate
echo "yes" | python3 manage.py collectstatic
python3 manage.py runserver 0.0.0.0:8080
